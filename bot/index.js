// 加入機器人
// https://discordapp.com/oauth2/authorize?&client_id=751001770557308968&scope=bot&permissions=8
const discord = require("discord.js");
const client = new discord.Client();
const config = require("../config/bot.json");
const { queryDB } = require("../mongoDB");
const admin = {
    '742297458432016426': { // brian
        userId: '742297458432016426',
        isCheck: false
    },
}
const frontEndMember = {
    '326769388449431552': { // jojo
        userId: '326769388449431552',
        isCheck: false
    },
    '499963028620574740': { // 東群
        userId: '499963028620574740',
        isCheck: false
    },
    '384947967901040640': { // 脩華
        userId: '384947967901040640',
        isCheck: false
    },
    '470792975945957417': { // 家佳
        userId: '470792975945957417',
        isCheck: false
    },
}
let message = null;
let timer = null;

client.on("ready",()=>{
    console.log(`成功登入囉!${client.user.tag}`);
});


client.on("message", async (msg) => {
    if (['general', '前端養身群'].indexOf(msg.channel.name) === -1) return;
    
    this.message = msg;
    const userData = msg.author;
    const member = frontEndMember[`${userData.id}`];
    const isAdmin = Object.keys(admin).indexOf(userData.id) > -1;
    const date = new Date(msg.createdTimestamp);
    
    console.log('channel: ', msg.channel.name);
    console.log('time: ', `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`);
    console.log('author: ', userData.username);
    console.log('isBot: ', userData.bot);
    console.log('content: ', msg.content);
    console.log(msg.attachments);
    console.log('-----------------------------------');
    
    if (msg.content === 'hello') {
        msg.channel.send(`:thumbsup: 收到！`);
        msg.channel.send('``` test \n qwe ```');
    }
    
    if (msg.content === 'all done' && isAdmin) {
        clearTimeout(timer);
        resetMember();
        msg.channel.send(':raised_hand: 已停止所有監聽');
    }

    if (msg.content.indexOf('done') > -1){
        member && (member.isCheck = true);
        if (isAllDone()) {
            clearTimeout(timer);
            msg.channel.send(':thumbsup: 水喔！ 大家都填寫好了！');
            resetMember();
        }
    }

    if (userData.bot && msg.content.indexOf('工作進度') > -1) {
        const result = await getRemindMessage();
        setTimeoutEvent(`${result.reply}`, msg, 1000 * 60 * 10);
    }

})

client.login(config.token);

async function getRemindMessage() {
    const result = await queryDB('bot', {});
    const randomNum = Math.floor(Math.random() * result.length);
    return result[randomNum];
    
}

function setTimeoutEvent(message, msg, sec) {
    timer = setTimeout(async () => {
        if (!isAllDone()) {
            Object.values(frontEndMember).forEach((user) => {
                !user.isCheck && msg.channel.send(`<@!${user.userId}> ${message}`);
            });
            const result = await getRemindMessage();
            setTimeoutEvent(`${result.reply}`, msg, 1000 * 60 * 5);
        }
    }, sec);
}

function isAllDone() {
    return Object.values(frontEndMember).every(user => user.isCheck);
}

function resetMember() {
    Object.values(frontEndMember).map((user) => {
        user.isCheck = false;
        return user;
    });
}
