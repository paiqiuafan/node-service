var express = require('express');
var path = require('path');
var indexPage = `${path.resolve(__dirname,'..')}/public/index.html`;

module.exports = () => {
    const router = express.Router()
    router.get('/', function (req, res) {
        res.sendFile(indexPage);
    });
    return router;
}