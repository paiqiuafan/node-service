const fetch = require('node-fetch');
const FormData = require('form-data');
const fs = require('fs');
const tokenUrl = 'https://notify-bot.line.me/oauth/token';
const notifyUrl = 'https://notify-api.line.me/api';

module.exports = {
    fetchToken: async (data) => {
        const { code, client_id, client_secret } = data;
        const form_data = new FormData();

        form_data.append('grant_type', 'authorization_code');
        form_data.append('code', code);
        form_data.append('redirect_uri', process.env.REDIRECT_URI);
        form_data.append('client_id', client_id);
        form_data.append('client_secret', client_secret);

        const res = await fetch(tokenUrl, {
            method: 'post',
            body: form_data
        });

        return res.json();
    },
    revokeToken: async (data) => {
        const { access_token } = data;
        const res = await fetch(`${notifyUrl}/revoke`, {
            headers: {
                Authorization: `Bearer ${access_token}`
            },
            method: 'post',
        });
        return res.json();
    },
    sendNotify: async (data) => {
        const { token, msg, imageFile } = data;
        const form_data = new FormData();
        console.log(imageFile);
        form_data.append('message', msg);
        // form_data.append('imageFile', imageFile);

        const res = await fetch(`${notifyUrl}/notify`, {
            headers: {
                Authorization: `Bearer ${token}`
            },
            method: 'post',
            body: form_data
        });
        
        return res.json();
    },
    sendNotifyFormData: async (data) => {
        const { files, body: { message, access_token } } = data;
        const form_data = new FormData();
        
        form_data.append('message', message);
        if (files) {
            form_data.append('imageFile', Buffer.from(files.imageFile.data), { filename: files.imageFile.name });
        }

        const res = await fetch(`${notifyUrl}/notify`, {
            headers: {
                Authorization: `Bearer ${access_token}`
            },
            method: 'post',
            body: form_data
        });

        return res.json();
    },
    tokenStatus: async (data) => {
        const { access_token } = data;
        const res = await fetch(`${notifyUrl}/status`, {
            headers: {
                Authorization: `Bearer ${access_token}`
            },
            method: 'get'
        });
        
        return res.json();
    }
}