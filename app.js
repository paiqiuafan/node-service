require('dotenv').config();
var express = require('express');
var app = express();
var cors = require('cors');
var fs = require('fs');
var bodyParser = require('body-parser');
var multer  = require('multer');
var upload = multer();
const fileUpload = require('express-fileupload');

const { resSuccess, resError } = require('./utils/apiResponse');

const routers = require('./routers/index.js')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.json());
app.use(cors());
// enable files upload
// app.use(upload.array());
app.use(fileUpload({
    createParentPath: true
}));

const PORT = process.env.PORT || 5000
const { mongo, queryDB, insertDB, COLLECTION } = require("./mongoDB");
const { queryMDB, insertMDB, removeMDB, M_COLLECTION, updateMDB } = require('./mongoDB/messages');
const { sendNotify, sendNotifyFormData, fetchToken, revokeToken, tokenStatus } = require("./line/notify");

function getIPAddress(){
    var interfaces = require('os').networkInterfaces();
    for(var devName in interfaces){
        var iface = interfaces[devName];
        for(var i=0;i<iface.length;i++){
            var alias = iface[i];
            if(alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal){
                return alias.address;
            }
        }
    }
}

app.get('/bot/reply/list', async (req, res) => {
    const result = await queryDB(COLLECTION.BOT, {});
    res.send(result);
});

app.get('/bot/reply/list/:id', async (req, res) => {
    const { id } = req.params;
    const result = await queryDB(COLLECTION.BOT, { _id: new mongo.ObjectID(id) });
    res.send(result);
});

app.post('/bot/reply/insert', async (req, res) => {
    const { keyword, reply } = req.body;
    if (reply) {
        const result = await insertDB(COLLECTION.BOT, { keyword, reply });
        const { insertedCount } = result;
        res.send({ message: 'success', insertedCount });
    } else {
        res.status(404).send({ message: 'require reply' });
    }
});

app.get('/line/userList', async (req, res) => {
    const { provider_name } = req.headers;
    const payload = {
        query: { provider_name },
        projection: {}
    };
    const userListRes = await queryMDB(M_COLLECTION.USERS, payload);
    res.send(userListRes);
});

app.get('/line/providerInfo', async (req, res) => {
    const { provider_name } = req.headers;
    const payload = {
        query: { provider_name },
        projection: { password: 0 }
    };
    const providerInfoRes = await queryMDB(M_COLLECTION.PROVIDER, payload);
    if (providerInfoRes.length === 1) {
        res.send(resSuccess(providerInfoRes[0]));
    } else {
        res.send(resError({ message: 'Login error' }));
    }
});

app.post('/line/login', async (req, res) => {
    const { account, password } = req.body;
    const payload = {
        query: { account, password },
        projection: { password: 0 }
    };
    const loginRes = await queryMDB(M_COLLECTION.PROVIDER, payload);
    if (loginRes.length === 1) {
        res.send(resSuccess(loginRes[0]));
    } else {
        res.send(resError({ message: 'Login error' }));
    }
});

// 暫時給sanae-store使用
app.post('/notify/line', async (req, res) => {
    const result = await sendNotify(req.body);
    res.send({ message: result.message });
});

app.post('/line/notify', async (req, res) => {
    const { token } = req.body;
    const result = await sendNotify(req.body);
    const { status, message } = result;
    if (status === 200) {
        res.send(result);
    } else {
        await removeMDB(M_COLLECTION.USERS, {access_token: token});
        res.send({message});
    }
});

app.post('/line/notifyFormData', async (req, res) => {
    const { provider_name } = req.body;
    const result = await sendNotifyFormData(req);
    const { status, message } = result;
    if (status === 200) {
        const payload = {
            query: { provider_name },
            update: { $inc: { current: 1 } }
        };
        await updateMDB(M_COLLECTION.PROVIDER, payload);
        res.send(result);
    } else {
        res.send({message});
    }
});

app.post('/line/fetchToken', async (req, res) => {
    const result = await fetchToken(req.body);
    res.send(result);
});

app.post('/line/revokeToken', async (req, res) => {
    const result = await revokeToken(req.body);
    res.send(result);
});

app.post('/line/removeToken', async (req, res) => {
    const { access_token } = req.body;
    const result = await removeMDB(M_COLLECTION.USERS, {access_token});
    res.send(result);
});

app.post('/line/recive', upload.array(), async (req, res) => {
    const { code, state } =  req.body;
    const payload = {
        query: { provider_name: state },
        projection: {}
    };
    const providerRes = await queryMDB(M_COLLECTION.PROVIDER, payload);
    if (!providerRes.length) res.send({});

    const { client_id, client_secret } = providerRes[0];
    const tokenRes = await fetchToken({ code, client_id, client_secret });
    if (tokenRes.status !== 200) res.send({});

    
    const { access_token } = tokenRes;
    const statusRes = await tokenStatus({ access_token });
    const { target, targetType } = statusRes;
    const insertRes = await insertMDB(M_COLLECTION.USERS, {
        access_token,
        provider_name: state,
        target_type: targetType,
        target,
        create_date: new Date()
    });
    res.send(insertRes);
});

app.use('/', routers());

app.listen(PORT, function () {
    console.log('Game IP:', `${getIPAddress()}:${PORT}`);
    console.log(`Example app listening on port ${PORT}!`);
});
