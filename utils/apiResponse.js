let res = {
    status: 200
}

exports.resSuccess = (data) => {
    return Object.assign({}, res, { data: data });
}

exports.resError = (data) => {
    return Object.assign({}, { status: 400, message: data.message });
}