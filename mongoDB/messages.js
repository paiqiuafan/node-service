// Require mongodb
const MongoClient = require('mongodb').MongoClient;
const username = process.env.DB_USER;
const password = process.env.DB_PASSWORD;
const hosts = process.env.DB_HOST;
const database = 'line';
const options = '?retryWrites=true&w=majority';
const connectionString = 'mongodb+srv://' + username + ':' + password + '@' + hosts + '/' + database + options;

const connectDB = () => {
    return MongoClient.connect(connectionString, { useUnifiedTopology: true });
} 

module.exports = {
    M_COLLECTION: {
        USERS: 'users',
        PROVIDER: 'provider'
    },
    queryMDB: async (collection, { query, projection } = payload) => {
        const db = await connectDB();
        const dbo = db.db(database);
        const res = await dbo.collection(collection).find(query, { projection }).toArray();
        db.close();
        return res;
    },
    insertMDB: async (collection, data) => {
        const db = await connectDB();
        const dbo = db.db(database);
        const res = await dbo.collection(collection).insertOne(data);
        db.close();
        return res;
    },
    updateMDB: async (collection, { query, update } = payload) => {
        const db = await connectDB();
        const dbo = db.db(database);
        const res = await dbo.collection(collection).updateOne(query, update);
        db.close();
        return res;
    },
    removeMDB: async (collection, data) => {
        const db = await connectDB();
        const dbo = db.db(database);
        const res = await dbo.collection(collection).remove(data);
        db.close();
        return res;
    }
}