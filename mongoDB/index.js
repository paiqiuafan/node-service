const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/';
const DB_NAME = {
    DEFAULT: 'test'
}

function connectDB() {
    return MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
}

module.exports = {
    COLLECTION: {
        BOT: 'bot'
    },
    mongo: require('mongodb'),
    queryDB: async (collection, query) => {
        const db = await connectDB();
        const dbo = db.db(DB_NAME.DEFAULT);
        const res = await dbo.collection(collection).find(query).toArray();
        db.close();
        return res;
    },
    insertDB: async (collection, data) => {
        const db = await connectDB();
        const dbo = db.db(DB_NAME.DEFAULT);
        const res = await dbo.collection(collection).insertOne(data);
        db.close();
        return res;
    }
}